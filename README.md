# Shops - The Ultimate Server Shop Plugin
![Shops Logo](https://cdn.kiranhart.com/spigot/shops/Header.png)
Ever wanted a shops plugin which you can setup categorized server shops with ease? Well this is the plugin you've been looking for. It was designed to be very easy to setup. Simply make a new shop, enable it, then add an item, and you're done (granted you've setup the permissions).

Note: This plugin has only been tested on the base Spigot jar. There is no guarantee that it will work on other distributions (ex. Paper) nor or they supported.

Requirements: Vault + An Economy Plugin (Ex. Essentials)

![](https://cdn.kiranhart.com/spigot/shops/gif/buy.gif)![](https://cdn.kiranhart.com/spigot/shops/gif/edit.gif)
![](https://cdn.kiranhart.com/spigot/shops/gif/openshop.gif)![](https://cdn.kiranhart.com/spigot/shops/gif/price.gif)
![](https://cdn.kiranhart.com/spigot/shops/gif/receipt.gif)![](https://cdn.kiranhart.com/spigot/shops/gif/rename.gif)
![](https://cdn.kiranhart.com/spigot/shops/gif/discord.png)

![Key Features](https://cdn.kiranhart.com/spigot/shops/KeyFeatures.png)

![Commands](https://cdn.kiranhart.com/spigot/shops/Commands.png)

* /shops open <name>
* /shops create <name>
* /shops remove <name>
* /shops list
* /shops edit <name>
* /shops additem <name> <sell> <buy>
* /shops setIcon <name>

![Permissions](https://cdn.kiranhart.com/spigot/shops/Permissions.png)

* Shops - Base Command
* Shops.admin - Admin Command
* Shops.cmds.* - Permission all Commands
* Shops.cmds.help - Help Command Permission
* Shops.cmds.create - Create Command Permission
* Shops.cmds.remove - Remove Command Permission
* Shops.cmds.list - Permission to list all the shops
* Shops.cmds.additem - Permission to add items to shops
* Shops.cmds.seticon - Permission to change shop icons.


> Buy purchasing this plugin you're agreeing that you will not redistribute this plugin without proper permission. Reselling this plugin is not allowed under any circumstance unless given explicit permission me, Kiran Hart to do so. You also acknowledge the fact that there is no refunds. Charge-backs will simply have you removed from the buyer list (you won't be able to download it again). You are allowed to use this plugin on as many servers as you wish. One purchase is valid for as many servers once you're the owner. If there is any issues please contact me through PMs before leaving low reviews. Posting entire error logs or giving low reviews without seeking help first will result in no support until they're removed.
